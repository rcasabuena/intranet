const express = require("express");
const router = express.Router();

/* Controllers */
const { logoutGet } = require("../controllers/AuthController");
const { dashboard } = require("../controllers/DasboardController");

/* Routes */
const usersRoutes = require("./users");
const registerRoutes = require("./register");
const loginRoutes = require("./login");
const resetPasswordRoutes = require("./reset-password");
const forgotPasswordRoutes = require("./forgot-password");

module.exports = () => {
  /* Dashboard */
  router.get("/", dashboard);

  /* Users Routes */
  router.use("/users", usersRoutes());

  /* Register */
  router.use("/register", registerRoutes());

  /* Login */
  router.use("/login", loginRoutes());

  /* Password Reset */
  router.use("/reset-password", resetPasswordRoutes());

  /* Forgot Password */
  router.use("/forgot-password", forgotPasswordRoutes());

  /* Logout */
  router.get("/logout", logoutGet);

  return router;
};
