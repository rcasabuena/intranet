const User = require("../models/User");

const mongoErrors = {
  11000: {
    email: "Email already registered",
    username: "Username already registered",
  },
};

const handleErrors = (err) => {
  let errors = {};
  if (undefined != err.errors) {
    Object.values(err.errors).forEach(({ properties }) => {
      errors[properties.path] = properties.message;
    });
  } else {
    errors[Object.keys(err.keyValue)[0]] =
      mongoErrors[err.code][Object.keys(err.keyValue)[0]];
  }
  return errors;
};

const registerGet = (req, res, next) => {
  res.render("auth/register", { layout: "auth" });
};

const registerPost = async (req, res, next) => {
  const { email, password, username } = req.body;
  try {
    const newUser = await User.create({ email, password, username });
    res.status(200).json(newUser);
  } catch (err) {
    const errors = handleErrors(err);
    res.status(400).json({ errors });
  }
};

const resetPasswordGet = (req, res, next) => {
  res.render("auth/reset-password", { layout: "auth" });
};

const resetPasswordPost = (req, res, next) => {
  // reset password form submit
  res.send("Password reset post");
};

const forgotPasswordGet = (req, res, next) => {
  res.render("auth/forgot-password", { layout: "auth" });
};

const forgotPasswordPost = (req, res, next) => {
  // forgot password form submit
  res.send("Forgot password post");
};

const loginGet = (req, res, next) => {
  res.render("auth/login", { layout: "auth" });
};

const loginPost = (req, res, next) => {
  // Login form submit
  res.send("Login post");
};

const logoutGet = (req, res, next) => {
  res.send("logout");
};

module.exports = {
  registerGet,
  registerPost,
  resetPasswordGet,
  resetPasswordPost,
  forgotPasswordGet,
  forgotPasswordPost,
  loginGet,
  loginPost,
  logoutGet,
};
