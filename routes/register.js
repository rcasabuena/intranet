const express = require("express");
const { registerGet, registerPost } = require("../controllers/AuthController");

const router = express.Router();

module.exports = () => {
  router.get("/", registerGet);
  router.post("/", registerPost);

  return router;
};
