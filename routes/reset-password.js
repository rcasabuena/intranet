const express = require("express");
const {
  resetPasswordGet,
  resetPasswordPost,
} = require("../controllers/AuthController");

const router = express.Router();

module.exports = () => {
  router.get("/", resetPasswordGet);
  router.post("/", resetPasswordPost);

  return router;
};
