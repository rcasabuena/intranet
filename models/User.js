const mongoose = require("mongoose");
const { isEmail, equals } = require("validator");
const bcrypt = require("bcrypt");

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: [true, "Please enter an email"],
      unique: true,
      lowercase: true,
      validate: [{ validator: isEmail, msg: "Please enter a valid email" }],
    },
    username: {
      type: String,
      required: [true, "Please enter username"],
      unique: true,
      minlength: [6, "Please enter a username of 6 or more characters"],
    },
    password: {
      type: String,
      required: [true, "Please enter a password"],
      minlength: [6, "Please enter a password of 6 or more characters"],
    },
  },
  { timestamps: true }
);

userSchema.post("save", (doc, next) => {
  console.log("user saved", doc);
  next();
});

userSchema.pre("save", async function (next) {
  const salt = await bcrypt.genSalt();
  this.password = await bcrypt.hash(this.password, salt);
  next();
});

const User = mongoose.model("user", userSchema);

module.exports = User;
