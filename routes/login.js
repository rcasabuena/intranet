const express = require("express");
const { loginGet, loginPost } = require("../controllers/AuthController");

const router = express.Router();

module.exports = () => {
  router.get("/", loginGet);
  router.post("/", loginPost);

  return router;
};
