const express = require("express");
const {
  forgotPasswordGet,
  forgotPasswordPost,
} = require("../controllers/AuthController");

const router = express.Router();

module.exports = () => {
  router.get("/", forgotPasswordGet);
  router.post("/", forgotPasswordPost);

  return router;
};
